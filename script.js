// 1. setTimeOut задает время через которое выполниться действия, а setInterval интрвал с которым оно будет выполняться
// 2. Функция будет выполнена как можно быстрее, но только после загрузки кода
// 3. Для того, чтобы остановить выполнение действия

const container = [...document.querySelectorAll('.image-to-show')]
const stop = document.querySelector('.button-1')
const play = document.querySelector('.button-2')

const createSlider = (time = 3000) => {

    const handleChangeSlide = () => {
        let currentIndex = container.findIndex(image => image.classList.contains('active'));
        const lastIndex = container.length - 1;

        if (currentIndex === lastIndex) {
            currentIndex = 0;
        } else {
            currentIndex++;
        }

        handleNextSlide(currentIndex);
    }

    const handleNextSlide = (idx) => {
        container.forEach(image => image.classList.remove('active'))
        container[idx].classList.add('active')
    }

    const slider = setInterval(() => {
        handleChangeSlide();
    }, time);

    const handleStopSlider = () => {
        stop.setAttribute('disabled', '');
        play.removeAttribute('disabled');
        return clearInterval(slider);
    };
    const handlePlayAgain = () => {
        stop.removeAttribute('disabled');
        return createSlider();
    };


    play.setAttribute('disabled', 'disabled');
    play.addEventListener('click', handlePlayAgain)
    stop.addEventListener('click', handleStopSlider)

}


createSlider();
